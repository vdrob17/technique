<?php

namespace App\technique;

use Katzgrau\KLogger\Logger;

class Loger
{
    const LOG_PATH = 'var/log';

    private $logger;

    public function __construct()
    {
        $this->logger = new Logger(self::LOG_PATH);
    }

    public function logInfo($message = "")
    {
        $this->logger->info($message);
    }

    public function logError($message = "")
    {
        $this->logger->error($message);
    }
}