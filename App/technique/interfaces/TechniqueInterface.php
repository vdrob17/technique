<?php

namespace App\technique\interfaces;
interface TechniqueInterface
{
    public function isConnectedToInternet();
    public function connectToInternet();
    public function sendData($obj);
    public function info();
}