<?php

namespace App\technique\device;

use App\technique\Technique;

class Smartphone extends Technique
{
    protected $PhoneNumber;
    protected $tableName = "Smartphones";


    public function __get($field)
    {
        if ($field == 'name') {
            return $this->name;
        }
        if ($field == 'oc') {
            return $this->oc;
        }
        if ($field == 'data') {
            return $this->data;
        }
        if ($field == 'connectedToInternet') {
            return $this->connectedToInternet;
        }
        if ($field == 'PhoneNumber') {
            return $this->PhoneNumber;
        }
    }
    public  function getDataArray()
    {
        return[
            'name' => $this->name,
            'oc' => $this->oc,
            'data' => $this->data,
            'PhoneNumber '=> $this->PhoneNumber,
            'ConnectedToInternet' => $this->connectedToInternet
        ];
    }
    public function toCall($number)
    {
        echo "<p>call to $number\n</p>";
    }

    public function info()
    {
        echo "<p>Smartphone $this->name on $this->oc OC ,phone number is $this->PhoneNumber with data->'$this->data'</p>";
    }

}