<?php

namespace App\technique\device;
use App\technique\Technique;
class Tablet extends Technique
{
    protected $tableName = "Tablets";

    public function getDataArray()
    {
        return[
            'name' => $this->name,
            'oc' => $this->oc,
            'data' => $this->data,
            'ConnectedToInternet' => $this->connectedToInternet
        ];
    }
    public function to_draw($picture)
    {
        echo "draw  $picture\n";
    }
    public function info()
    {
        echo "Tablet $this->name on $this->oc OC  with data-> '$this->data'";
    }
}