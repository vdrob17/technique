<?php

namespace App\technique\device;
use App\technique\Technique;
use App\technique\EmptyValue;

class Computer extends Technique
{
    protected $tableName = 'Computers';



    public function getDataArray()
    {
        return[
            'name' => $this->name,
            'oc' => $this->oc,
            'data' => $this->data,
            'ConnectedToInternet' => $this->connectedToInternet
        ];
    }

    public function info()
    {
        echo "<p>Computer $this->name on $this->oc OC with data ->'$this->data'</p>";
    }

}