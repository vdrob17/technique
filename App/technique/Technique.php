<?php

namespace App\technique;

use App\Service\DBase;
use App\technique\interfaces\TechniqueInterface;

abstract class Technique implements TechniqueInterface
{
    protected $name;
    protected $oc;
    protected $connectedToInternet;
    protected $data;

    protected $tableName;

    public function __get($field)
    {
        if ($field == 'name') {
            return $this->name;
        }
        if ($field == 'oc') {
            return $this->oc;
        }
        if ($field == 'data') {
            return $this->data;
        }
        if ($field == 'connectedToInternet') {
            return $this->connectedToInternet;
        }

    }

    public function __set($field, $value)
    {
        if ($field == 'name') {
            $this->name = $value;
        }
        if ($field == 'oc') {
            $this->oc = $value;
        }
        if ($field == 'data') {
            $this->data = $value;
        }
        if ($field == 'connectedToInternet') {
            $this->connectedToInternet = $value;
        }
    }

    public function __toString()
    {
        return "</p>device:$this->name,oc:$this->oc,with data($this->data) </p>";
    }

    public function isConnectedToInternet()
    {
        if ($this->connectedToInternet == true) {
            return true;
        } else
            return false;
    }

    public function disconnectToInternet()
    {
        // echo "</p>$this->name disconnected to the internet\n</p>";
        // $loger = new Loger();
        // $loger->logInfo("$this->name disconnected to the internet");
        $this->connectedToInternet = false;
    }

    public function setField(array $data)
    {
        foreach ($data as $k => $v) {
            $this->$k = $v;
        }
    }

    public function getDataArray()
    {

    }
    public function GetFieldById($id)
    {
        $database = DBase::DBConnection();
        $device = "App\\Service\\DBase::DBGetTable$this->tableName";
        $data = $database->select($this->tableName, $device(), [
            'id' => $id
        ]);

        $data = json_encode($data);
        $arr = json_decode($data, true);
        return $arr[0];
    }
    public function save()
    {
        $database = DBase::DBConnection();
        $database->insert($this->tableName, $this->getDataArray());
    }

    public function read()
    {
        $database = DBase::DBConnection();
        $data = $database->select($this->tableName, $this->getDataArray());
        $data = json_encode($data);
        $arr = json_decode($data, true);
        $this->setField($arr);
        $this->info();
    }

    public function delete($id)
    {
        $database = DBase::DBConnection();
        $data = $database->delete($this->tableName, [
            'id' => $id
        ]);
    }
    public function edit($id)
    {
        $database = DBase::DBConnection();
        $database->update($this->tableName, $this->getDataArray(),[
            'id' => $id
        ]);
    }
    public function connectToInternet()
    {
        //$loger = new Loger();
        //$loger->logInfo("$this->name connected to the internet");
        //echo "</p>$this->name connected to the internet\n</p>";
        $this->connectedToInternet = true;
    }

    public function readData()
    {
        echo "</p>data of $this->name is $this->data\n</p>";
    }


    public function sendData($obj)
    {
        if (($this->isConnectedToInternet() == true) && ($obj->isConnectedToInternet() == true)) {
            $obj->data = $this->data;
        } else echo "</p>Problem with connection\n</p>";
    }


}