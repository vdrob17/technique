<?php


namespace App\Service;

use Medoo\Medoo;

class DBase
{
    private static $connection = null;

    public static function DBConnection()
    {
        if (is_null(self::$connection)) {
            self::$connection = new Medoo([
                'database_type' => 'mysql',
                'database_name' => 'drobko',
                'server' => 'localhost',
                'username' => 'root',
                'password' => 'drobko'
            ]);
        }
        return self::$connection;
    }


    public static function DBGetTableComputers()
    {
           return[
               'name' ,
               'oc' ,
               'data',
               'ConnectedToInternet'
           ];
    }
    public static function DBGetTableSmartphones()
    {
        return[
            'name' ,
            'oc' ,
            'data',
            'PhoneNumber',
            'ConnectedToInternet'
        ];
    }
    public static function DBGetTableTablets()
    {
        return[
            'name' ,
            'oc' ,
            'data',
            'ConnectedToInternet'
        ];
    }
    public static function DBGetComputers()
    {
        $database = self::DBConnection();
        $data = $database->select('Computers', [
            'id',
            'name',
            'oc',
            'data',
            'ConnectedToInternet'
        ]);
        $data = json_encode($data);
        $arr = json_decode($data, true);
        return $arr;
    }

    public static function DBGetTablets()
    {
        $database = self::DBConnection();
        $data = $database->select('Tablets', [
            'id',
            'name',
            'oc',
            'data',
            'ConnectedToInternet'
        ]);
        $data = json_encode($data);
        $arr = json_decode($data, true);
        return $arr;
    }

    public static function DBGetSmartphones()
    {
        $database = self::DBConnection();
        $data = $database->select('Smartphones', [
            'id',
            'name',
            'oc',
            'data',
            'PhoneNumber',
            'ConnectedToInternet'
        ]);
        $data = json_encode($data);
        $arr = json_decode($data, true);
        return $arr;
    }
}