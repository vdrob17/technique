<?php

namespace App\Service;

use App\Controller\Edit;
use App\Controller\EditForm;
use App\Controller\Home;
use App\Controller\Create;
use App\Controller\Data;
use App\Controller\Delete;
use App\Controller\Read;
use App\Controller\Send;

use Klein\Klein;

class Router
{
    public static function dispatch()
    {
        $klein = new Klein();
        $klein->respond("GET", '/', function () {
            $controller = new Home();
            $controller->execute();
        });
        $klein->respond("GET", '/index.php/read', function () {
            $controller = new Read();
            $controller->execute();
        });
        $klein->respond("GET", '/index.php/create', function () {
            $controller = new Create();
            $controller->execute();
        });
        $klein->respond("POST", '/index.php/data', function () {
            $controller = new Data();
            $controller->execute();
        });
        $klein->respond("GET", '/index.php/delete/[:id][:type]', function ($request) {
            $controller = new Delete();
            $controller->execute($request);
        });
        $klein->respond("GET", '/index.php/edit/[:id][:type]', function ($request) {
            $controller = new EditForm();
            $controller->execute($request);
        });
        $klein->respond("POST", '/index.php/edit/[:id]', function ($request) {
            $controller = new Edit();
            $controller->execute($request);
        });
        $klein->dispatch();
    }
}