<?php


namespace App\Controller;


use App\Service\DBase;

class Delete
{
    public function execute($request)
    {
        $type = $request->type;
        $id = $request->id;

        $deviceName = "\\App\\technique\\device\\" . substr($type, 0, -1);

        $device = new $deviceName();
        $arr = $device->GetFieldById($id);
        $device->setField($arr);
        $device->edit($id);
        echo "<p> device was deleted</p>";
        echo "<a href='/'>Home</a>";
    }
}