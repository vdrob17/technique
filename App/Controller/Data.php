<?php


namespace App\Controller;

use App\Service\DBase;
use App\technique\device\Computer;
use App\technique\device\Smartphone;
use App\technique\device\Tablet;


class Data
{
    public function execute()
    {
        $deviceName = "\\App\\technique\\device\\".$_POST['device'];
        $device = new $deviceName();

        $device->setField($_POST);
        $device->info();
        $device->save();


        echo "<p>New device was added</p>";
        echo "<a href='/'>Home</a>";
    }
}