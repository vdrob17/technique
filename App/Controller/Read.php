<?php


namespace App\Controller;

use App\Service\DBase;
use App\technique\device\Computer;
use App\technique\device\Smartphone;
use App\technique\device\Tablet;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class Read
{
    public function execute()
    {
        $loader = new FilesystemLoader(__DIR__ . '/templates');
        $twig = new Environment($loader);
        echo $twig->render('Home.html.twig');
        $arr = DBase::DBGetComputers();
        echo "<p>Computers:<p>";
        for ($i = 0; $i < count($arr); $i++) {
            $id = $arr[$i]['id'];
            $type = 'Computers';
            echo "<div><a href='/index.php/delete/$id$type'> delete </a>";
            echo "<a href='/index.php/edit/$id$type'> edit </a></div>";
            $device[$i] = new Computer();
            $device[$i]->setField($arr[$i]);
            $device[$i]->info();
        }
        echo "<p>Smarphones:<p>";
        $arr = DBase::DBGetSmartphones();
        for ($i = 0; $i < count($arr); $i++) {
            $id = $arr[$i]['id'];
            $type = 'Smartphones';
            echo "<div><a href='/index.php/delete/$id$type'> delete </a>";
            echo "<a href='/index.php/edit/$id$type'> edit </a></div>";
            $device[$i] = new Smartphone();
            $device[$i]->setField($arr[$i]);
            $device[$i]->info();
        }
        $arr = DBase::DBGetTablets();
        echo "<p>Tablets:<p>";
        for ($i = 0; $i < count($arr); $i++) {
            $id = $arr[$i]['id'];
            $type = 'Tablets';
            echo "<div><a href='/index.php/delete/$id$type'> delete </a>";
            echo "<a href='/index.php/edit/$id$type'> edit </a></div>";
            $device[$i] = new Tablet();
            $device[$i]->setField($arr[$i]);
            $device[$i]->info();
        }
        /* for ($i = 0; $i < count($arr); $i++) {
             $id = $arr[$i]['id'];
             echo $id;
             echo "<div><a href='/index.php/delete&amp;=$id'> delete </a>";
             echo "<a href='/index.php/edit&amp;=$id'> edit </a></div>";
             $device[$i]->info();

             echo "";
         }
         echo "<form method='post' action='/index.php/send'>";
         $name = $device[$i]->name;
         echo "<select name='sender' >";
         for ($j = 0; $j < count($arr); $j++) {
             $val = $arr[$j]['id'];
             $cap = $arr[$j]['name'];
             echo "<option value='$val'>$cap</option>";
         }
         echo "</select>";
         echo "<input type='submit' value='Send'>";
         echo "<select name='recipient'>";
         for ($j = 0; $j < count($arr); $j++) {
             $val = $arr[$j]['id'];
             $cap = $arr[$j]['name'];
             echo "<option value='$val'>$cap</option>";
         }
         echo "</select>";
         echo "</form>";*/
    }
}