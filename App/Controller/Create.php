<?php


namespace App\Controller;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;


class Create
{
    public function execute()
    {
        $loader = new FilesystemLoader(__DIR__ . '/templates');
        $twig = new Environment($loader);
        echo $twig->render('Home.html.twig');
        echo $twig->render('Form.html.twig');
    }
}