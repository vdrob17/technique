<?php


namespace App\Controller;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;


use App\Service\DBase;

class EditForm
{
    public function execute($request)
    {
        $loader = new FilesystemLoader(__DIR__ . '/templates');
        $twig = new Environment($loader);
        $type = $request->type;
        $id = $request->id;
        $deviceName = "\\App\\technique\\device\\" . substr($type, 0, -1);
        $device = new $deviceName();
        $arr = $device->GetFieldById($id);
        echo $twig->render('EditForm.html.twig',
            [
                'id' => $id,
                'type' => $type,
                'name' => $arr['name'],
                'oc' => $arr['oc'],
                'data' => $arr['data'],
                'phoneNumber' => $arr['phoneNumber'],
                'ConnectedToInternet' => $arr['ConnectedToInternet']
            ]);
    }
}